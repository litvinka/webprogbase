const Playlist = require('../models/playlist');

 
class PlaylistRepository 
{
 

 
    async getPlaylists() 
    { 
        try
        {
            const playlists = await Playlist.find({});

            return playlists;

        }
        catch(err)
        {
            console.log(err);
        }
        
    }
 
    async getPlaylistById(id) 
    {
        try
        {
            const playlist = await Playlist.findById(id).populate('movies',['title']).populate('user_id', ['login']);

            
        
            return playlist;

        }
        catch(err)
        {
            console.log(err);
        }
        
    }

    async addPlaylist(playlistModel)
    {
        try
        {
            const playlist = new Playlist({
                name: playlistModel.name, 
                user_id: playlistModel.user_id,
                movies: playlistModel.movies
                
            });
    
            const result = await playlist.save();
            
     
            
            return result.id;

        }
        catch(err)
        {
            console.log(err);
        }
        



    }
    async updatePlaylist(playlistModel)
    {
        try
        {
            const playlist = new Playlist({
                _id: playlistModel._id,
                name: playlistModel.name, 
                movies: playlistModel.movies
                
            });
            
            await Playlist.findByIdAndUpdate(playlistModel._id, playlist);

        }
        catch (err)
        {
            console.log(err);

        }
        
       
        

    }
    async deletePlaylist(id)
    {
        try
        {
            const deleted = await Playlist.findByIdAndDelete(id);
            return deleted;

        }
        catch(err)
        {
            console.log(err);
        }
        


        


    }
    async getUserPlaylists(userId)
    {
        const user_playlists = await Playlist.find({user_id: userId});
        
        
        return user_playlists;

    }
};
 
module.exports = PlaylistRepository;