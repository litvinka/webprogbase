const router = require('express').Router();

const userRouter = require('./users');
const movieRouter = require('./movies');
const playlistRouter = require('./playlists');


const aboutRouter = require('./about');

const apiController = require('../controllers/api');

router.use('/users', userRouter);
router.use('/movies', movieRouter);

router.use('/about', aboutRouter);
router.use('/playlists', playlistRouter);


router.get('/', apiController.load_home_page);

module.exports = router;
