
const Playlist= require('../models/playlist');

const PlaylistRepository = require('../repositories/playlistRepository');
const MovieRepository = require('../repositories/movieRepository');
const UserRepository = require('../repositories/userRepository');

const movieRepository = new MovieRepository();
const playlistRepository = new PlaylistRepository();
const userRepository = new UserRepository;

module.exports = 
{
    async createPlaylist(req, res, next) 
    {
        try
        {   
            const new_name = req.body.name;

            const new_user_id = req.body.user_id;

            const new_movies = req.body.movies;

            
            
            const playlist = new Playlist({
                name: new_name, 
                user_id: new_user_id,
                movies: new_movies
                
                
            });
            
            new_id = await playlistRepository.addPlaylist(playlist);

            path = 'playlists/' + new_id;
            
            res.redirect(path);
            
        }
        catch(err)
        {
            next(err);
            

        }
        
    },
    
    async deletePlaylistById(req, res, next) {

        try
        {
            const playlist_id = req.params.id;
        
        
            const deleted = await playlistRepository.deletePlaylist(playlist_id);

            const user_id = deleted.user_id;
            
            

            res.redirect(`/users/${user_id}`);

        }
        catch(err)
        {
            next(err);
        }

        
            

        
        




    },

    

    async load_playlist_page(req,res, next)
    {   
        
        try
        {
            const id = req.params.id;
           
            const playlist = await playlistRepository.getPlaylistById(id);
            
            res.render('playlist', playlist);

        }
        catch(err)
        {
            next(err);
        }
        
    },

    async load_new_playlist_page(req,res,next)
    {   
        try
        {
            const movies = await movieRepository.getMovies();
            const user_id = req.params.user_id;
            const user = await userRepository.getUserById(user_id);
            res.render('new_playlist', {movies, user});

        }
        catch(err)
        {
            next(err);

        }
        

    }
    

};