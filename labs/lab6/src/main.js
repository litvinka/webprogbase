import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { createProvider } from './vue-apollo'

Vue.config.productionTip = false

const app = new Vue({
  router,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')

export {app};