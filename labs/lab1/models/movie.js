class Movie 
{

        constructor(id, title, director,rating,releasedAt,posterUrl,budget) {
        this.id = id;  // number
        this.title = title;  // string
        this.director = director;  // string
        this.rating = rating;  // number
        this.releasedAt = releasedAt;  // ISO 8601 date
        this.posterUrl= posterUrl;  // string
        this.budget = budget;  // number, in million dollars
         
    }
    
        
    
    
 };
 
 module.exports = Movie;
 