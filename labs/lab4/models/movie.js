
/**
 * @typedef Movie
 * @property {string} id
 * @property {string} title.required - title of the movie
 * @property {string} director.required - fullname of the movie director
 * @property {integer} rating - Rotten Tomatoes rating
 * @property {string} releasedAt.required - date of movie release
 * @property {string} posterUrl - URL adress of the movie poster
 * @property {number} budget.required - movie budget in million dollars
 */

const mongoose = require('mongoose');
const MovieSchema = require('./../schemas/movie');


 const MovieModel = mongoose.model('Movie', MovieSchema);
 
 
 
 
 module.exports = MovieModel;
 