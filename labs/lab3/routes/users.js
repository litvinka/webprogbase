const router = require('express').Router();

const userController = require('../controllers/users');

router

/**
 * Get user model by id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */

    .get('/:id(\\d+)', userController.load_user_page)

    /**
 * Get a page with a number of users
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query.required - page number
 * @param {integer} per_page.query.required - items per page
 * @returns {Array.<User>} 200 - a page with users
 * @returns {Error} 404 - page doesn't exist
 * @returns {Error} 400 - invalid query
 */

    .get("/", userController.load_users_page);
   
module.exports = router;

