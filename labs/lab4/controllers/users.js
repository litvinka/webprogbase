
const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository();

const PlaylistRepository = require('../repositories/playlistRepository');
const playlistRepository = new PlaylistRepository();

module.exports = 
{

    async getUsers(req, res, next) 
    {
        try
        {

            
            
            res.set("Content-type", "application/json");
            if (!('page' in req.query))
            {
                res.status(400);
                res.json({ });
                return;
            }
        
            const page = Number.parseInt(req.query.page);
            if(isNaN(page))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if(page <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if (!('per_page' in req.query))
            {
                res.status(400);
                res.json({ });
                return;
            }
        
            const per_page = Number.parseInt(req.query.per_page);
            if(isNaN(per_page))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if(per_page <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if(per_page > 100)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            const users = await userRepository.getUsers();

            
            
            
            const items_total = users.length;
            
            const pages_total = Math.ceil(items_total/per_page);
            if(page > pages_total)
            {
                res.status(404);
                res.json({ });
                return;
            }

            const first_index = per_page * (page-1);
            let i;
            let page_users = new Array();
            

            for(i = first_index;i < first_index + per_page; i++)
            {

                if(i > (items_total - 1))
                {
                    break;
                }
                
                page_users.push(users[i]);


            } 

        
            res.status(200);
            
            res.json(page_users);
        }


        catch(err)
        {
            next(err);
        }


        
         
    },

    async getUserById(req, res, next) 
    {

        try
        {

        
            const id = req.params.id;
            
            res.set("Content-type", "application/json");

            
            const user = await userRepository.getUserById(id);
            
            
            
            if(user !== null)
            {
                
                res.status(200);
                
                res.json(user);

            }
            else
            {
                
                res.status(404);
                res.json({ });

                
                
            }
        }

        catch(err)
        {
            next(err);
        }

    },

    async load_users_page(req,res,next)
    {   try
        {
            const users = await userRepository.getUsers();
            
            res.render('users', {users});

        }
        catch(err)
        {
            
            next(err);

        }
        
    },

    async load_user_page(req,res, next)
    {
        try
        {
            const id = req.params.id;
            const user = await userRepository.getUserById(id);

            const playlists = await playlistRepository.getUserPlaylists(id);
            
            res.render('user', {user, playlists});

        }
        catch(err)
        {
            
            next(err);

        }
        
    }
    

    
};

