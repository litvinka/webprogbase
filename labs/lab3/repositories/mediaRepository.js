const Media = require('../models/media');
const JsonStorage = require('../jsonStorage');

const mime = require('mime-types');

class MediaRepository 
{
 
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }

    getMedias() 
    { 
        const jsonObject = this.storage.readItems();
        return jsonObject.items;  
    }

    getMediaById(id) 
    {
        const items = this.getMedias();
        const new_id = items.findIndex(item => item.id === id);
        
            if (new_id !== -1) 
            {
                const media_file = new Media(items[new_id].id,items[new_id].path);
                
                           

                return media_file;
            
            }
        return null;
    }

    addMedia(file)
    {
        const id = this.storage.nextId;
        const ext = mime.extension(file.mimetype);
        const filename = 'file' + id + '.' + ext;
        const filepath = __dirname + `/../data/media/${filename}`;
        fs.writeFileSync(`${filepath}`, file.data,{flag : "w"});


        const media_file = new Media(id,filepath);
        this.storage.incrementNextId();
        const mediasObject = this.storage.readItems();
        mediasObject.items.push(media_file);
        this.storage.writeItems(mediasObject);
        return media_file.id;



    }

    deleteMedia(id)
    {
        const media_to_delete = this.getMediaById(id);
        const path_to_delete = media_to_delete.path;
        fs.unlinkSync(path_to_delete);

        console.log('deleting');
        const mediaObject = this.storage.readItems();
        const index_to_delete = mediaObject.items.findIndex(x => x.id === id);
        mediaObject.items.splice(index_to_delete,1);
        this.storage.writeItems(mediaObject);



        


    }
    get_next_id()
    {
        const jsonObject = this.storage.readItems();
        return jsonObject.nextId;  

    }
 

}  
module.exports = MediaRepository;