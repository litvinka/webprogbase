const router = require('express').Router();

const userRouter = require('./users');
const movieRouter = require('./movies');
const mediaRouter = require('./media');
const aboutRouter = require('./about');

const apiController = require('../controllers/api');

router.use('/users', userRouter);
router.use('/movies', movieRouter);
router.use('/media', mediaRouter);
router.use('/about', aboutRouter);


router.get('/', apiController.load_home_page);

module.exports = router;
