function pushNotify(title, id) {
    new Notify({
      status: 'success',
      title: `A new movie ${title} has been added`,
      text: null,
      effect: 'fade',
      speed: 300,
      customClass: null,
      customIcon: `<a href = '/movies/${id}' target='_blank"'>link</a>`,
      showIcon: true,
      showCloseButton: true,
      autoclose: true,
      autotimeout: 7000,
      gap: 20,
      distance: 20,
      type: 2,
      position: 'right top'
    })
  }