const Movie = require('../models/movie');

class MovieRepository 
{
 

 
    async getMovies() 
    { 
       
            const movies = await Movie.find({});

            return movies;

        
    }
 
    async getMovieById(id) 
    {
        
            const movie = await Movie.findById(id);
        
            return movie;

        
       
        
    }

    async addMovie(movieModel)
    {
       
            const movie = new Movie({
                title: movieModel.title, 
                director: movieModel.director,
                rating: movieModel.rating,
                releasedAt: movieModel.releasedAt,
                posterUrl: movieModel.posterUrl,
                budget: movieModel.budget
            });
    
            const result = await movie.save();
            
     
            
            return result.id;

        



    }
    async updateMovie(movieModel)
    {
          const movie = new Movie({
                _id: movieModel._id,
                title: movieModel.title, 
                director: movieModel.director,
                rating: movieModel.rating,
                releasedAt: movieModel.releasedAt,
                posterUrl: movieModel.posterUrl,
                budget: movieModel.budget
            });
            
            await Movie.findByIdAndUpdate(movieModel._id, movie);

        
        
        
       
        

    }
    async deleteMovie(id)
    {
        
            await Movie.findByIdAndDelete(id);

        
        
        


        


    }
};
 
module.exports = MovieRepository;