const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    login: {type: String, required: true},
    fullname: {type: String},
    role: {type: Number, required: true},
    registeredAt: {type: Date, required: true},
    avaUrl: {type: String},
    isEnabled: {type: Boolean, required: true},
    bio: {type: String},

 });

 module.exports = UserSchema;