const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation)



connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', () => console.error(`ws error`));
connection.addEventListener('message', (message) => {
    
    
    const movieString = message.data;
    const movie = JSON.parse(movieString);
    const id = movie.id;
    const title = movie.title;
    pushNotify(title, id);

    
});

const xhttp = new XMLHttpRequest();

let template;
const template_listener = function(e) {
    const target = e.target;
    template = target.responseText;
    

   
    
    
 };
xhttp.addEventListener('load', template_listener);

xhttp.open("GET", "/templates/movies_list.mst", false);
xhttp.send();

xhttp.removeEventListener('load', template_listener);

const spinner = document.getElementById("spinner");

const next_btn = document.getElementById("next_btn");

const prev_btn = document.getElementById("prev_btn");

const search_btn = document.getElementById("search_btn");

const page_num = document.getElementById("page_number");

const pages_total = document.getElementById("pages_total");

const table = document.getElementById("movies_table");

const err_message = document.getElementById("err_message");

const search_str = document.getElementById("search_str");

const hidden_search = document.getElementById("hidden_search");

const all_mov_cap = document.getElementById("all_movies_caption");

const no_mov_cap = document.getElementById("no_movies_caption");

const what_mov_cap = document.getElementById("what_movies_caption");








xhttp.addEventListener('load', function(e) {
    const target = e.target;
    
    
    if(target.status === 200)
    {
        
        const rows = table.rows;
        
        for(let i = rows.length -1; i > 0;i--)
        {
            table.deleteRow(i);
        }
        const moviesString = target.responseText;
        const moviesObject = JSON.parse(moviesString);
        const movies = moviesObject.movies;
        pages_total.innerText = moviesObject.total;
        if(pages_total.innerText == '0')
        {
            pages_total.innerText = '1';
            next_btn.setAttribute('disabled', 'disabled');
            prev_btn.setAttribute('disabled', 'disabled');
            
            if(hidden_search.value)
            {
                all_mov_cap.setAttribute('hidden', 'hidden');
                what_mov_cap.setAttribute('hidden', 'hidden');
                no_mov_cap.removeAttribute('hidden');
                no_mov_cap.innerText = "No movies that contain " + hidden_search.value + " found";

            }
            else
            {
                search_btn.setAttribute('disabled', 'disabled');
                all_mov_cap.removeAttribute('hidden');
                no_mov_cap.setAttribute('hidden', 'hidden');
                what_mov_cap.setAttribute('hidden', 'hidden');

            }
            


        }
        else
        {
            if(hidden_search.value)
            {
                all_mov_cap.setAttribute('hidden', 'hidden');
                no_mov_cap.setAttribute('hidden', 'hidden');
                what_mov_cap.removeAttribute('hidden');
                what_mov_cap.innerText = "Movies that contain " + hidden_search.value;

            }
            else
            {
                all_mov_cap.removeAttribute('hidden');
                no_mov_cap.setAttribute('hidden', 'hidden');
                what_mov_cap.setAttribute('hidden', 'hidden');

            }

        }
        
        for(const movie of movies)
        {
            const renderedHtmlStr = Mustache.render(template, {movie: movie});
            const row = document.createElement("TR");
            
            row.innerHTML = renderedHtmlStr;
            table.appendChild(row);
    


        }
        
        
    }
    else if (target.status !== 404)
    {
        
        err_message.removeAttribute("hidden");
    }
    if(prev_btn)
    {
        if(page_num.value == 1)
        {
            prev_btn.setAttribute("disabled","disabled");
        }
        if(page_num.value == pages_total.innerText)
        {
            next_btn.setAttribute("disabled","disabled");
        }
    }
    

    
    spinner.style.visibility = 'hidden';
    
    
    
 });
 if(hidden_search)
 {

    
    document.addEventListener("DOMContentLoaded", function(event) {
        
        let page;
        if(page_num)
        {
            page = page_num.value;
            
        }
        else
        {
            page = 1;


        }
        
        

        const per_page = 5;
        

        xhttp.open("GET", "/movies/?page=" + page + "&per_page=" + per_page + "&search=" + hidden_search.value,true );
                
        xhttp.send();

        spinner.style.visibility = 'visible';
    });
 }

  if(prev_btn)
  {
    prev_btn.addEventListener('click', function(e){
        next_btn.removeAttribute('disabled');
        page_num.value = parseInt(page_num.value) - 1;
        const page = page_num.value;
        const per_page = 5;
        xhttp.open("GET", "/movies/?page=" + page + "&per_page=" + per_page + "&search=" + hidden_search.value,true );
            
        xhttp.send();

        spinner.style.visibility = 'visible';



    });

  }

  if(next_btn)
  {
    next_btn.addEventListener('click', function(e){
        prev_btn.removeAttribute('disabled');
        page_num.value = parseInt(page_num.value) + 1;
        const per_page = 5;
        const page = page_num.value;
        
        xhttp.open("GET", "/movies/?page=" + page + "&per_page=" + per_page + "&search=" + hidden_search.value, true );
            
        xhttp.send();

        spinner.style.visibility = 'visible';



    });

  }
  
  if(search_btn)
  {

    search_btn.addEventListener('click', function(e){

        hidden_search.value = search_str.value;
        
        next_btn.removeAttribute('disabled');
        page_num.value = 1;
        const per_page = 5;
        const page = page_num.value;
        
        xhttp.open("GET", "/movies/?page=" + page + "&per_page=" + per_page + "&search=" + hidden_search.value, true );
            
        xhttp.send();

        spinner.style.visibility = 'visible';

        
        



    });

  }
  

 