const mongoose = require('mongoose');

 const MovieSchema = new mongoose.Schema({
    title: {type: String, required: true},
    director: {type: String, required: true},
    rating: {type: Number, required: true, min: 0, max: 100},
    releasedAt: {type: Date, required: true},
    posterUrl: {type: String},
    budget: {type: Number, required: true}
});

module.exports = MovieSchema;