
const apiRouter = require('./routes/api');
const express = require('express');
const app = express();

const busboy = require('busboy-body-parser');

const options_busboy = {
   limit: '5mb',
   multi: false,
};

app.use(busboy(options_busboy));

const bodyParser = require('body-parser');
app.use(bodyParser.json({ extended: true }));


app.use('/api', apiRouter);

const port = 3000

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'JSON HTTP API server for movies',
            title: 'Movies server',
            version: '1.0.0',
        },
        host: `localhost:${port}`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);


app.use(function(req, res) {  console.log('Any request');  });

app.listen(port, function() { console.log('Server is ready');});









