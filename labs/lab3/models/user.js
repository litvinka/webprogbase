
/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - name and surname of the user
 * @property {integer} role.required - 1 for admins, 0 for regular users
 * @property {string} registeredAt.required - date of user registration
 * @property {string} avaUrl - URL adress of profile picture
 * @property {boolean} isEnabled.required - shows if profile is active
 */

class User 
{

        constructor(id, login, fullname,role,registeredAt,avaUrl,isEnabled, bio) 
    {
        this.id = id;  // integer
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;  // integer
        this.registeredAt = registeredAt;  // ISO 8601 date
        this.avaUrl = avaUrl;  // string
        this.isEnabled = isEnabled;  // boolean
        this.bio = bio;
         
    }
    
        
    
    
 };
 
 module.exports = User;
 