/**
 * @typedef Playlist
 * @property {string} _id
 * @property {string} name.required - name of the playlist
 * @property {string} user_id.required - id of the creator of the playlist
 * @property {array} movies - ids of the movies in the playlist
 */

const mongoose = require('mongoose');
const PlaylistSchema = require('../schemas/playlist');

const PlaylistModel = mongoose.model('Playlist', PlaylistSchema);
 
 
 
 
 module.exports = PlaylistModel;
