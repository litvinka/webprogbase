const WebSocketServer = require('ws').Server;

class WsServer {
    constructor(server) {
        this.connections = [];
        this.wsServer = new WebSocketServer({ server: server });
        this.wsServer.on('connection', (connection) => {
            this.addConnection(connection);
        
            connection.on('close', () => this.removeConnection(connection));
            connection.on('message', (message) => {
                const dataStr = message.toString();
               
                this.notifyAll(dataStr, connection);
                
            });
 
        });
       
    }
 
    addConnection(connection) { this.connections.push(connection); }
    removeConnection(connection) { this.connections = this.connections.filter(x => x !== connection); }
 
    notifyAll(text, conn) {
        
        for (const connection of this.connections) { 
           if(connection !== conn)
           {
               connection.send(text); 

           }
        }
    }
 };
 
 module.exports = WsServer;