
const mongoose = require('mongoose');

const mustache = require('mustache-express');

const path = require('path');

const apiRouter = require('./routes/api');
const express = require('express');

const app = express();

const busboy = require('busboy-body-parser');

const options_busboy = {
   limit: '5mb',
   multi: false,
};

app.use(busboy(options_busboy));

const bodyParser = require('body-parser');
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));

const morgan = require('morgan');
app.use(morgan('dev'));

app.use('/', apiRouter);



const expressSwaggerGenerator = require('express-swagger-generator');
const cons = require('consolidate');
const expressSwagger = expressSwaggerGenerator(app);
 
const config = require('./config');

const host = '0.0.0.0';
const port = config.port;
const options = {
    swaggerDefinition: {
        info: {
            description: 'JSON HTTP API server for movies',
            title: 'Movies server',
            version: '1.0.0',
        },
        host: `localhost:${port}`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.use(express.static('public'));

const viewsDir = path.join(__dirname, 'views');
const partialsDir = path.join(viewsDir, 'partials');




app.engine('mst', mustache(partialsDir));

app.set('views', viewsDir);
app.set('view engine', 'mst');



const dbUrl = config.dbConnectionString;
const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};






app.use(function(req, res) {  console.log('Any request');  });

app.listen(port,host, async() => 
{   try
    {
        console.log('Server is ready');

        client = await mongoose.connect(dbUrl, connectOptions);

        
     
        console.log('Database connected');
        
    }
    catch(err)
    {
        console.log("Database connection failed");
        console.log(err);
    }
    

});

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something went wrong!');
 })
 








