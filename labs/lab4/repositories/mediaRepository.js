

const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

async function uploadRaw(buffer)
{
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader
        .upload_stream(
          { resource_type: 'raw' }, 
          (err, result) => {
            if (err) {
              reject(err);
            } else {
              resolve(result);
            }
          })
        .end(buffer);
  });
}

async function deleteRaw(id)
{
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader
        .destroy(id,
          { resource_type: 'raw' }, 
          (err, result) => {
            if (err) {
              reject(err);
            } else {
              resolve(result);
            }
          })
        ;
  });
}


class MediaRepository 
{
 
    async addMedia(file)
    {
        try
        {
            const result = await uploadRaw(file);
            
            return result.url;

        }
        catch(err)
        {
            console.log(err);
        }
        




    }

    async deleteMedia(id)
    {
         try
         {
             await deleteRaw(id);
            
         }
         catch(err)
         {
            console.log(err);

         }


    }

   

}  
module.exports = MediaRepository;