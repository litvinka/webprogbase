
function _arrayBufferToBase64( buffer ) {
    let binary = '';
    let bytes = new Uint8Array( buffer );
    let len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
}

const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation)


connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', () => console.error(`ws error`));
connection.addEventListener('message', (message) => {
    
    
    const movieString = message.data;
    const movie = JSON.parse(movieString);
    const id = movie.id;
    const title = movie.title;
    pushNotify(title, id);

    
});


const xhttp = new XMLHttpRequest();

let template;
const template_listener = function(e) {
    const target = e.target;
    template = target.responseText;

   
    
    
 };
xhttp.addEventListener('load', template_listener);

xhttp.open("GET", "/templates/last_created.mst", false);
xhttp.send();

xhttp.removeEventListener('load', template_listener);

const err_message = document.getElementById("err_message");
 
const new_mov_form = document.getElementById("new_mov_form");

const no_movies = document.getElementById("no_last_movies");

const submit = document.getElementById("submit");

const spinner = document.getElementById("spinner");


xhttp.addEventListener('load', function(e) {
    const target = e.target;
    
    if(target.status === 201)
    {
        if(no_movies)
        {
            no_movies.setAttribute("hidden","hidden");

        }
        
        const movieString = target.responseText;
        const movie = JSON.parse(movieString);
        connection.send(movieString);
        const renderedHtmlStr = Mustache.render(template, movie);
        
        const added_list = document.getElementById("last_added");
        const list_element = document.createElement("LI");   
        list_element.innerHTML = renderedHtmlStr;

        const list_items = added_list.getElementsByTagName("li");
        if(list_items.length === 5)
        {
            list_items[4].remove();

        }

        
        added_list.insertBefore(list_element, added_list.childNodes[2]); // child nodes 0 and 1 is caption
        

    }
    else
    {
        err_message.removeAttribute("hidden");
    }
    

    submit.removeAttribute("disabled");
    spinner.style.visibility = 'hidden';
    
    
 });
 
    new_mov_form.addEventListener("submit", async function(e)
    {
        
        submit.setAttribute("disabled","disabled");
        err_message.setAttribute("hidden","hidden");
        spinner.style.visibility = 'visible';
        e.preventDefault();
        const new_title = e.target.title.value;
        const new_director = e.target.director.value;
        const new_release_date = e.target.release_date.value;
        const new_budget = e.target.budget.value;
        const new_rating = e.target.rating.value;
        const new_posterUrl =  _arrayBufferToBase64(await e.target.photoFile.files[0].arrayBuffer());
        const file_type = e.target.photoFile.files[0].type;
        const extension = file_type.slice(6);
        
        
        const new_createdAt = new Date();

        const movie = {
            title: new_title,
            director: new_director,
            releasedAt: new_release_date,
            budget: new_budget,
            rating: new_rating,
            posterUrl: new_posterUrl,
            createdAt: new_createdAt,
            ext: extension
            
        };
        const movieObject = JSON.stringify(movie);


       
        
        xhttp.open("POST", "/movies",true );
        xhttp.setRequestHeader("Content-type",        
        "application/json");
        xhttp.send(movieObject);




    



    });

 


