fs = require('fs');
const MediaRepository = require('../repositories/mediaRepository');
const mediaRepository = new MediaRepository('./data/media.json');
module.exports = {

    createMedia(req, res) 
    {
        try
        {
            res.set("Content-type", "application/json");
            const next_id = mediaRepository.get_next_id().toString();
            const filename = 'file' + next_id + '.jpg';
            const filepath = __dirname + `/../data/media/${filename}`;
            fs.writeFileSync(`${filepath}`, req.files['file-key'].data,{flag : "w"});

            mediaRepository.addMedia(filepath);
            
            res.status(201);
            res.json({id:next_id });
        

        }
        catch(e)
        {
            res.status(400);
            res.json({ });
        }
        
        

        
    },
   
    getMediaById(req, res) 
    {
        const id = parseInt(req.params.id);
        
        res.set("Content-type", "application/json");
        const media_file = mediaRepository.getMediaById(id);
        if(media_file !== null)
        {
            res.status(200);
            
            res.download(media_file.path);


        }
        else
        {
            
            res.status(404);
            res.json({ });

            
            
        }


        
    },

    
};
