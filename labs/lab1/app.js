const UserRepository = require('./repositories/userRepository');

const MovieRepository = require('./repositories/movieRepository');
 
const userRepository = new UserRepository('./data/users.json');

const movieRepository = new MovieRepository('./data/movies.json');

const readline = require('readline-sync');
const Movie = require('./models/movie');



while (true) 
{
    const input = readline.question("Enter command:");
    console.log(" ");
    const command = input.split("/");

    if (input === "get/users") 
    {
        const users = userRepository.getUsers();
        for(const user of users)
        {
            if(user.role === 1)
            {
                console.log(`Admin: id: ${user.id}, login: ${user.login}, fullname: ${user.fullname}`);
            }
            else
            {
                console.log(`User: id: ${user.id}, login: ${user.login}, fullname: ${user.fullname}`);
            }
            
        }
       
    }
    else if (input === "get/movies") 
    {
        const movies = movieRepository.getMovies();
        for(const movie of movies)
        {
            const release_date = new Date(Date.parse(movie.releasedAt));
            const release_year = release_date.getFullYear();
            console.log(`Movie: id: ${movie.id}, title: ${movie.title}, director: ${movie.director}, year of release: ${release_year}`);
            
        }
       
    }
    
    else if ((command[0] === "get")&&(command[1] === "user"))
    {
        const id = Number(command[2]);
        
        if (Number.isInteger(id) && (id > 0))
        {
            if(userRepository.getUserById(id) !== null)
            {
                const user = userRepository.getUserById(id);
                const reg_date = new Date(Date.parse(user.registeredAt));
                const readable_reg_date = reg_date.toDateString();
                    if(user.role === 1)
                    {
                        
                        console.log(`id: ${user.id}`);
                        console.log(`login: ${user.login},`);
                        console.log(`fullname: ${user.fullname},`);
                        console.log(`role: admin,`);
                        console.log(`date of registration: ${readable_reg_date},`);
                        console.log(`ava url: ${user.avaUrl},`);
                        console.log(`enabled: ${user.isEnabled}`);

                    }
                    else
                    {
                        
                        console.log(`id: ${user.id}`);
                        console.log(`login: ${user.login},`);
                        console.log(`fullname: ${user.fullname},`);
                        console.log(`role: user,`);
                        console.log(`date of registration: ${readable_reg_date},`);
                        console.log(`ava url: ${user.avaUrl},`);
                        console.log(`enabled: ${user.isEnabled}`);

                    }
                    
                }
                else
                {
                    console.log("No user with such id found");
                }


        }
        else
        {
            console.log("Incorrect id. Please try again.")
        }



    }
    else if ((command[0] === "get")&&(command[1] === "movie"))
    {
        const id = Number(command[2]);
        
        if (Number.isInteger(id) && (id > 0))
        {
            if(movieRepository.getMovieById(id) !== null)
            {
                const movie = movieRepository.getMovieById(id);
                const rel_date = new Date(Date.parse(movie.releasedAt));
                const readable_rel_date = rel_date.toDateString();
                console.log(`id: ${movie.id}`);
                console.log(`title: ${movie.title},`);
                console.log(`director: ${movie.director},`);
                console.log(`date of release: ${readable_rel_date},`);
                console.log(`RT rating: ${movie.rating}`);
                console.log(`poster url: ${movie.posterUrl},`);
                console.log(`budget: ${movie.budget} million $`);
            }
                else
                {
                    console.log("No movie with such id found");
                }


        }
        else
        {
            console.log("Incorrect id. Please try again.")
        }



    }
    else if ((command[0] === "delete")&&(command[1] === "movie"))
    {
        const id = Number(command[2]);
        
        if (Number.isInteger(id) && (id > 0))
        {
            if(movieRepository.getMovieById(id) !== null)
            {
                const movie = movieRepository.getMovieById(id);
                while(true)
                {
                    const on_delete = readline.question(`Are you sure you want to delete "${movie.title}"? (yes/no): `);
                    if(on_delete === "yes")
                    {
                        movieRepository.deleteMovie(id);
                        break;
                    }
                    else if (on_delete === "no")
                    {
                        break;
    
                    }
                    else
                    {
                        console.log("Incorrect command. Please type 'yes' or 'no'");

                    }

                }
                
            }
                else
                {
                    console.log("No movie with such id found");
                }


        }
        else
        {
            console.log("Incorrect id. Please try again.")
        }



    }

    else if ((command[0] === "update")&&(command[1] === "movie"))
    {
        const id = Number(command[2]);
        
        if (Number.isInteger(id) && (id > 0))
        {
            if(movieRepository.getMovieById(id) !== null)
            {
                
                while(true)
                {
                    const movie = movieRepository.getMovieById(id);
                    const rel_date = new Date(Date.parse(movie.releasedAt));
                    const readable_rel_date = rel_date.toDateString();
                    
                    console.log(`id: ${movie.id}`);
                    console.log(`title: ${movie.title},`);
                    console.log(`director: ${movie.director},`);
                    console.log(`date of release: ${readable_rel_date},`);
                    console.log(`RT rating: ${movie.rating}`);
                    console.log(`poster url: ${movie.posterUrl},`);
                    console.log(`budget: ${movie.budget} million $`);
                    console.log(" ");
                    
                    const on_update = readline.question(`What information about "${movie.title}" do you wish to edit? Type 'back' to stop editing: ` );
                    if(on_update === "id")
                    {
                        console.log("You can't edit id");
                        
                    }
                    else if (on_update === "title")
                    {
                        const title_update = readline.question(`Enter new title: `);
                        movie.title = title_update;
                        movieRepository.updateMovie(movie);
                        
    
                    }
                    else if (on_update === "director")
                    {
                        const director_update = readline.question(`Enter new director: `);
                        movie.director = director_update;
                        movieRepository.updateMovie(movie);
                    }
                    else if (on_update === "date of release")
                    {
                        while(true)
                        {
                            const rel_date_update = readline.question(`Enter new date of release: `);
                            check_date = Date.parse(rel_date_update);
                            if(isNaN(check_date))
                            {
                                console.log("Invalid date. Please make sure yout input is in ISO 8601");
                            }
                            else
                            {
                                movie.releasedAt = rel_date_update;
                                movieRepository.updateMovie(movie);
                                break;
                            }

                        }
                        
    
                    }
                    else if (on_update === "RT rating")
                    {
                        while(true)
                        {
                            const rating_update = readline.question("Enter new rating: ");
                            const rating = Number(rating_update);
                            if((Number.isInteger(rating)) && (rating >= 0) && (rating <= 100))
                            {
                                movie.rating = rating;
                                movieRepository.updateMovie(movie);
                                break;
                            }
                            else
                            {
                                console.log("Invalid rating. Please make sure your input is an integer number between 0 and 100");
                            }
                        }
                        
    
                    }
                    else if (on_update === "poster url")
                    {
                        const poster_update = readline.question(`Enter new URL of poster: `);
                        movie.posterUrl = poster_update;
                        movieRepository.updateMovie(movie);
                        
    
                    }
                    else if (on_update === "budget")
                    {
                        while(true)
                        {
                            const budget_update = readline.question("Enter new budget: ");
                            const budget = Number(budget_update);
                            if((!isNaN(budget)) && (budget > 0))
                            {
                                movie.budget = budget;
                                movieRepository.updateMovie(movie);
                                break;
                            }
                            else
                            {
                                console.log("Invalid budget. Please make sure your input is positive number");
                            }
                        }
                    
    
                    }
                    else if (on_update === "back")
                    {
                        break;
                    
    
                    }
                    else
                    {
                        console.log("Incorrect command. Please try again");

                    }
                    console.log(" ");

                }
                
            }
                else
                {
                    console.log("No movie with such id found");
                }


        }
        else
        {
            console.log("Incorrect id. Please try again.")
        }



    }
    else if ((command[0] === "post")&&(command[1] === "movie"))
    {
        console.log("Posting a movie: ")
        const new_title = readline.question("Enter new movie title: ");
        const new_director = readline.question("Enter new movie director: ");
        var rating;
        while(true)
        {
            const new_rating = readline.question("Enter new movie rating: ");
            rating = Number(new_rating);
            if((Number.isInteger(rating)) && (rating >= 0) && (rating <= 100))
            {
                
                break;
            }
            else
            {
                console.log("Invalid rating. Please make sure your input is an integer number between 0 and 100");
            }
            
        }
        var new_rel_date;
        while(true)
        {
            new_rel_date = readline.question(`Enter new movie date of release: `);
            check_date = Date.parse(new_rel_date);
            if(isNaN(check_date))
            {
                console.log("Invalid date. Please make sure yout input is in ISO 8601");
            }
            else
            {
                break;
            }


        }
        const new_poster = readline.question("Enter new movie poster url: ");
        var budget;
        while(true)
        {
            const new_budget = readline.question("Enter new movie budget: ");
            budget = Number(new_budget);
            if((!isNaN(budget)) && (budget > 0))
            {
                
                break;
            }
            else
            {
                console.log("Invalid budget. Please make sure your input is positive number");
            }
        }
        const movie = new Movie(-1,new_title,new_director,rating,new_rel_date,new_poster,budget);
        movieRepository.addMovie(movie);

        

        
        

    }
    else if (input === "exit")
    {
        break;
       

    }

    else
    {
        console.log("Incorrect command. Please try again.");
    }

    console.log(" ");
}


