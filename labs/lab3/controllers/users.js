
const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');

module.exports = 
{

    getUsers(req, res) 
    {
        
        
        res.set("Content-type", "application/json");
        if (!('page' in req.query))
        {
            res.status(400);
            res.json({ });
            return;
        }
    
        const page = Number.parseInt(req.query.page);
        if(isNaN(page))
        {
            res.status(400);
            res.json({ });
            return;

        }

        if(page <= 0)
        {
            res.status(400);
            res.json({ });
            return;

        }
        if (!('per_page' in req.query))
        {
            res.status(400);
            res.json({ });
            return;
        }
    
        const per_page = Number.parseInt(req.query.per_page);
        if(isNaN(per_page))
        {
            res.status(400);
            res.json({ });
            return;
        }
        if(per_page <= 0)
        {
            res.status(400);
            res.json({ });
            return;

        }
        if(per_page > 100)
        {
            res.status(400);
            res.json({ });
            return;

        }
        
        const users = userRepository.getUsers();
        const items_total = users.length;
        
        const pages_total = Math.ceil(items_total/per_page);
        if(page > pages_total)
        {
            res.status(404);
            res.json({ });
            return;
        }

        const first_index = per_page * (page-1);
        var i;
        var page_users = new Array();
        

        for(i = first_index;i < first_index + per_page; i++)
        {

            if(i > (items_total - 1))
            {
                break;
            }
            
            page_users.push(users[i]);


        } 

     
        res.status(200);
        
        res.json(page_users);


        
         
    },

    getUserById(req, res) {
        const id = parseInt(req.params.id);
        
        res.set("Content-type", "application/json");
        const user = userRepository.getUserById(id);
        if(user !== null)
        {
            
            res.status(200);
            
            res.json(user);

        }
        else
        {
            
            res.status(404);
            res.json({ });

            
            
        }

    },

    load_users_page(req,res)
    {   
        const users = userRepository.getUsers();
        res.render('users', {users});
    },

    load_user_page(req,res)
    {
        const id = parseInt(req.params.id);
        const user = userRepository.getUserById(id);
        console.log(user);
        res.render('user', user);
    }
    

    
};

