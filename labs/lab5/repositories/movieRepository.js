const Movie = require('../models/movie');
const JsonStorage = require('../jsonStorage');
 
class MovieRepository 
{
 
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }
 
    getMovies() 
    { 
        const jsonObject = this.storage.readItems();
        return jsonObject.items;  
    }
 
    getMovieById(id) 
    {
        const items = this.getMovies();
        const new_id = items.findIndex(item => item.id === id);
        
            if (new_id !== -1) 
            {
                const movie = new Movie(items[new_id].id, items[new_id].title, items[new_id].director,items[new_id].rating,items[new_id].releasedAt,items[new_id].posterUrl,items[new_id].budget,items[new_id].createdAt);
                

                return movie;
            
            }
        return null;
    }

    addMovie(movieModel)
    {
        const movie = new Movie(-1, movieModel.title, movieModel.director,movieModel.rating,movieModel.releasedAt,movieModel.posterUrl,movieModel.budget,movieModel.createdAt);
        movie.id = this.storage.nextId;
        this.storage.incrementNextId();
        const moviesObject = this.storage.readItems();
        moviesObject.items.push(movie);
        this.storage.writeItems(moviesObject);
        return movie.id;



    }
    updateMovie(movieModel)
    {
        const moviesObject = this.storage.readItems();
        const index_to_update = moviesObject.items.findIndex(x => x.id === movieModel.id);
        moviesObject.items[index_to_update] = movieModel;
        this.storage.writeItems(moviesObject);
        

    }
    deleteMovie(id)
    {
        
        const moviesObject = this.storage.readItems();
        const index_to_delete = moviesObject.items.findIndex(x => x.id === id);
        moviesObject.items.splice(index_to_delete,1);
        this.storage.writeItems(moviesObject);


        


    }
};
 
module.exports = MovieRepository;