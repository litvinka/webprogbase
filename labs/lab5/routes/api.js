const router = require('express').Router();


const movieRouter = require('./movies');
const mediaRouter = require('./media');

router.use('/movies', movieRouter);
router.use('/media', mediaRouter);



module.exports = router;
