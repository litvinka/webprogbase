const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation)



connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', () => console.error(`ws error`));
connection.addEventListener('message', (message) => {
    
    
    const movieString = message.data;
    const movie = JSON.parse(movieString);
    const id = movie.id;
    const title = movie.title;
    pushNotify(title, id);

    
});
const xhttp = new XMLHttpRequest();


const delete_form = document.getElementById("delete_form");

const err_message = document.getElementById("err_message");
 
const submit = document.getElementById("submit");

const spinner = document.getElementById("spinner");

const modal = document.getElementById("myModal");

const span = document.getElementById("close");

const yes_btn = document.getElementById("yes_btn");

const cancel_btn = document.getElementById("cancel_btn");

const deleted = document.getElementById("deleted_message");




xhttp.addEventListener('load', function(e) {
    const target = e.target;
    
    if(target.status === 200)
    {
        deleted.removeAttribute("hidden");
        
    }
    else
    {
        submit.removeAttribute("disabled");
        err_message.removeAttribute("hidden");
    }
    

    
    spinner.style.visibility = 'hidden';
    
    
 });

    if(delete_form)
    {
        delete_form.addEventListener("submit", async function(e)
        {
            submit.setAttribute("disabled","disabled");
            err_message.setAttribute("hidden","hidden");
            
            e.preventDefault();
            modal.style.display = "block";
            
          

    



        });

    }

    if(span)
    {
        span.addEventListener("click",function() {
            modal.style.display = "none";
            submit.removeAttribute("disabled");
          });

    }
    
      
      
    window.addEventListener("click", function(event) {
    if (event.target == modal) {
          modal.style.display = "none";
          submit.removeAttribute("disabled");
    }})

    if(yes_btn)
    {
        yes_btn.addEventListener("click", function(e){
            const url = document.URL;
            
            const id_to_delete = url.slice(url.indexOf('movies') + 7);
            
            xhttp.open("DELETE", "/movies/" + id_to_delete ,true );
            
            xhttp.send();

            spinner.style.visibility = 'visible';

            modal.style.display = "none";


        });
    }
    
    if(cancel_btn)
    {
        cancel_btn.addEventListener("click", function(e){
           

            modal.style.display = "none";
            submit.removeAttribute("disabled");
            


        });
    }
