const router = require('express').Router();

const aboutController = require('../controllers/about')

router.get("/", aboutController.load_about_page)

module.exports = router;