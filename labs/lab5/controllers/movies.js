const moment = require('moment');
const converter = require('base64-arraybuffer');


const Movie = require('../models/movie');
const MovieRepository = require('../repositories/movieRepository');
const movieRepository = new MovieRepository('./data/movies.json');

const MediaRepository = require('../repositories/mediaRepository');

const mediaRepository = new MediaRepository('./data/media.json');

function compare(movie_a, movie_b) {
    if (movie_a.title < movie_b.title) {
      return -1;
    }
    if (movie_a.title > movie_b.title) {
      return 1;
    }
    
    return 0;
  }

function toBuffer(ab) {
    let buf = Buffer.alloc(ab.byteLength);
    let view = new Uint8Array(ab);
    for (let i = 0; i < buf.length; ++i) {
        buf[i] = view[i];
    }
    return buf;
}
module.exports = 
{

    getMovies(req, res, next) {
        
        try
        {

        
            res.set("Content-type", "application/json");
            if (!('page' in req.query))
            {
                res.status(400);
                res.json({ });
                return;
            }
        
            let page = Number.parseInt(req.query.page);
            if(isNaN(page))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if(page <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if (!('per_page' in req.query))
            {
                res.status(400);
                res.json({ });
                return;
            }
        
            const per_page = Number.parseInt(req.query.per_page);
            if(isNaN(per_page))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if(per_page <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if(per_page > 100)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            const searched_string = req.query.search;
            
            const all_movies = movieRepository.getMovies();
            let movies = [];
            if(searched_string != undefined)
            {
                for (const movie of all_movies)
                {
                
                    if(movie.title.toLowerCase().indexOf(searched_string.toLowerCase()) !== -1)
                    {
                        
                        movies.push(movie);

                    }
                    

                }
            }
            else
            {
                movies = all_movies
                

            }
            
            movies.sort(compare);
            const items_total = movies.length;
            
            const pages_total = Math.ceil(items_total/per_page);

            if(page > pages_total)
            {
                page = pages_total;
            }

            const first_index = per_page * (page-1);
            let i;
            let page_movies = new Array();
            

            for(i = first_index;i < first_index + per_page; i++)
            {

                if(i > (items_total - 1))
                {
                    break;
                }
                
                page_movies.push(movies[i]);


            } 

            let is_undefined = false;
            for(page_movie of page_movies)
            {
                if(typeof page_movie === 'undefined')
                {
                    is_undefined = true;
                    break;
                }
            }
            if(!(is_undefined))
            {
                movies = page_movies;
            }
            

        
            res.status(200);
            
            res.json({movies: movies, total:pages_total});
        }
        catch(err)
        {
            next(err);

        }


    },

    getMovieById(req, res, next) 
    {
        try
        {

        
            const id = parseInt(req.params.id);
            
            res.set("Content-type", "application/json");
            const movie = movieRepository.getMovieById(id);
            if(movie !== null)
            {
                res.status(200);
                
                res.json(movie);

            }
            else
            {
                
                res.status(404);
                res.json({ });

                
                
            }
        }
        catch(err)
        {
            next(err);
        }

    },

    createMovie(req, res, next) 
    {
        
        try
        {
            movie_object = req.body;
            
            if (!('title' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_title = movie_object.title;
            if (!('director' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_dir = movie_object.director;
            if (!('rating' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_rate = Number(movie_object.rating);
            if(!Number.isInteger(new_rate))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if((new_rate < 0)||(new_rate > 100))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if (!('releasedAt' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rel_date = movie_object.releasedAt;
            const check_date = Date.parse(new_rel_date);
            if(isNaN(check_date))
            {
                res.status(400);
                res.json({ });
                return;
            }

            if (!('createdAt' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            let new_cr_date = movie_object.createdAt;
            const check_cr_date = Date.parse(new_cr_date);
            if(isNaN(check_cr_date))
            {
                res.status(400);
                res.json({ });
                return;
            }


            if (!('posterUrl' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            
            
            const poster_data = toBuffer(converter.decode(movie_object.posterUrl));
            const extension = movie_object.ext;
            const poster_object = {
                data:poster_data,
                mimetype: `image/${extension}`

            };
            const new_poster_id =  mediaRepository.addMedia(poster_object);
            
           

            const new_poster = "/media/" + new_poster_id;
            if (!('budget' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_budget = Number(movie_object.budget);
            
            if(isNaN(new_budget))
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(new_budget <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            new_cr_date = moment(new_cr_date).format('MMMM Do YYYY, h:mm:ss a');
            
            
            
            const movie = new Movie(-1,new_title,new_dir,new_rate,new_rel_date,new_poster,new_budget,new_cr_date);

            new_id = movieRepository.addMovie(movie);
            movie.id = new_id;
            
            
            res.status(201);
            res.json(movie);
            
            

        }
        catch(e)
        {
            
            console.log(e);
            res.status(400);
            res.json({ });

        }
        
    },
    updateMovie(req, res) {
        try
        {
            movie_object = req.body;
            if (!('id' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const id = movie_object.id;
            if(!Number.isInteger(id))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if(id <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if (!('title' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_title = movie_object.title;
            if (!('director' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_dir = movie_object.director;
            if (!('rating' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rate = Number(movie_object.rating);
            if(!Number.isInteger(new_rate))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if((new_rate < 0)||(new_rate > 100))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if (!('releasedAt' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rel_date = movie_object.releasedAt
            const check_date = Date.parse(new_rel_date);
            if(isNaN(check_date))
            {
                res.status(400);
                res.json({ });
                return;
            }

            if (!('posterUrl' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_poster = movie_object.posterUrl;
            

            
            if (!('budget' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_budget = Number(movie_object.budget);
            
            if(isNaN(new_budget))
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(new_budget <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(movieRepository.getMovieById(id) === null)
            {
                res.status(404);
                res.json({ });
                return;

            }
            
            const movie = new Movie(id,new_title,new_dir,new_rate,new_rel_date,new_poster,new_budget);

            movieRepository.updateMovie(movie);
            
            
            res.status(200);
            
            res.json(movie);

        }
        catch(e)
        {
            
            console.log(e);
            res.status(400);
            res.json({ });

        }

    },
    deleteMovieById(req, res, next) {
        try
        {

        
            const id = parseInt(req.params.id);
            
            res.set("Content-type", "application/json");
            const movie = movieRepository.getMovieById(id);
            if(movie !== null)
            {
                const poster_path = movie.posterUrl;

                let media_id_str = poster_path[7]; //first character after '/media', always a number

                for(let i = 8; i < poster_path.length; i++)
                {
                    if(!isNaN(parseInt(poster_path[i])))
                    {
                        media_id_str = media_id_str + poster_path[i];
                        
                    }

                }
                const media_id = parseInt(media_id_str);

                mediaRepository.deleteMedia(media_id);
                res.status(200);
                
                movieRepository.deleteMovie(id);
                res.json(movie);
                

            }
            else
            {
                
                res.status(404);
                res.json({ });
            }
        }
        catch(err)
        {
            next(err);
        }
        




    },
    load_movie_page(req,res, next)
    {   
        try
        {
            const id = parseInt(req.params.id);
            const movie = movieRepository.getMovieById(id);
            res.render('movie', movie);
        }
        catch(err)
        {
            next(err);
        }
    },
    load_movies_page(req,res,next)
    {
        try
        {
            if (('per_page' in req.query))
            {
                return next();
            }

        
            let page;
            if (!('page' in req.query))
            {
                page = 1;
            }

            else
            {
                page = parseInt(req.query.page);

            }
        

            if (('next' in req.query))
            {
                page = page + 1;
            }

            if (('prev' in req.query))
            {   
                if(page !== 1)
                {
                    page = page - 1;
                }
            }
            const searched_string = req.query.title_search;
            
            const all_movies = movieRepository.getMovies();
            let movies = [];
            if(searched_string != undefined)
            {
                for (const movie of all_movies)
                {
                
                    if(movie.title.toLowerCase().indexOf(searched_string.toLowerCase()) !== -1)
                    {
                        
                        movies.push(movie);

                    }
                    

                }
            }
            else
            {
                movies = all_movies
                

            }
            
            movies.sort(compare);
            const items_total = movies.length;
            const per_page = 5;
            const pages_total = Math.ceil(items_total/per_page);

            if(page > pages_total)
            {
                page = pages_total;
            }

            const first_index = per_page * (page-1);
            let i;
            let page_movies = new Array();
            

            for(i = first_index;i < first_index + per_page; i++)
            {

                if(i > (items_total - 1))
                {
                    break;
                }
                
                page_movies.push(movies[i]);


            } 

            let is_undefined = false;
            for(page_movie of page_movies)
            {
                if(typeof page_movie === 'undefined')
                {
                    is_undefined = true;
                    break;
                }
            }
            if(!(is_undefined))
            {
                movies = page_movies;
            }
            
            
            
            
            res.render('movies', {movies,searched_string, page, pages_total});
        }
        catch(err)
        {
            next(err);

        }
    },
    load_new_movie_page(req,res,next)
    {   
        try
        {

            
            const movies = movieRepository.getMovies();
            
            const stop_number = movies.length - 6; //we have to get 5 last created movies

            let last_movies = [];

            for(let i = movies.length -1;i >=0; i--)
            {
                if(i === stop_number)
                {
                    break;
                }
                last_movies.push(movies[i]);
                




            }
            

            let cur_date_string = new Date().getFullYear().toString() + "-" + new Date().getMonth().toString()+ "-" + new Date().getDate().toString()
            const cur_date = {cur_date_string: cur_date_string};
            res.render('new_movie', {cur_date, last_movies});
        }   
        catch(err)
        {
            next(err);
        }

    }

    
};

