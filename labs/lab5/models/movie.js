
/**
 * @typedef Movie
 * @property {integer} id
 * @property {string} title.required - title of the movie
 * @property {string} director.required - fullname of the movie director
 * @property {integer} rating - Rotten Tomatoes rating
 * @property {string} releasedAt.required - date of movie release
 * @property {string} posterUrl - URL adress of the movie poster
 * @property {number} budget.required - movie budget in million dollars
 */

class Movie 
{

        constructor(id, title, director,rating,releasedAt,posterUrl,budget,createdAt) {
        this.id = id;  // integer
        this.title = title;  // string
        this.director = director;  // string
        this.rating = rating;  // integer
        this.releasedAt = releasedAt;  // ISO 8601 date
        this.posterUrl= posterUrl;  // string
        this.budget = budget;  // float or integer, in million dollars
        this.createdAt = createdAt; // ISO 8601 date
         
    }
    
        
    
    
 };
 
 module.exports = Movie;
 