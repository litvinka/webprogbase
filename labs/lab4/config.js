require('dotenv').config();
 
const config = {
    dbConnectionString: process.env.DB_CONNECTION_STRING,
    port: process.env.PORT,
    cloudinary: {
        cloud_name: process.env.CLOUD_NAME,
        api_key: process.env.API_KEY,
        api_secret: process.env.API_SECRET

    }
};
 
module.exports = config;