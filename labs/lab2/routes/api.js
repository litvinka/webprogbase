const router = require('express').Router();

const userRouter = require('./users');
const movieRouter = require('./movies');
const mediaRouter = require('./media');
router.use('/users', userRouter);
router.use('/movies', movieRouter);
router.use('/media', mediaRouter);

module.exports = router;
