import gql from 'graphql-tag'

export const LOGIN = gql`query login ($username: String!, $password: String!) {
    login(username: $username, password: $password)
      
    
   }`;



export const REGISTER = gql`query register ($username: String!, $password: String!) {
    register(username: $username, password: $password)
    {
        id
        username
        timestamp
    }
      
    
   }`;
   
export const ME = gql`query me {
    me
    {
        id
        username
        timestamp
        currentRoom
        {
            id
            name
            timestamp
            members{
                username
              }
            lastMessages{
                id
                author
                {
                  username
                }
                timestamp
                text
              }
            owner{
              username
            }
            
        }
    }
      
    
   }`;

export const ROOMS = gql`query rooms {
    rooms
    {
        id
        name
        owner { 
      username
     }

    }
      
    
   }`;

   export const ROOM_CREATED = gql`subscription roomCreated {
    roomCreated
    {
        id
        name
        owner { 
      username
     }

    }
      
    
   }`;

   export const ROOM_UPDATED = gql`subscription roomUpdated {
    roomUpdated
    {
        id
        name
        owner { 
      username
     }

    }
      
    
   }`;

   export const ROOM_DELETED = gql`subscription roomDeleted {
    roomDeleted
    {
        id
        name
        owner { 
      username
     }

    }
      
    
   }`;

   export const CREATE_ROOM = gql`mutation createRoom ($name: String!) {
    createRoom(name: $name)
    {
        id
        name
        timestamp
    }
      
    
   }`;

   export const JOIN_ROOM = gql`mutation joinRoom ($roomId: ID!) {
    joinRoom(roomId: $roomId)
    {
        id
        name
       
    }
      
    
   }`;

   export const ROOM_CHANGED = gql`subscription currentRoomChanged {
    currentRoomChanged
    {
        id
        username
        timestamp
        currentRoom
        {
            id
            name
            timestamp
            members{
                username
              }
            lastMessages{
                id
                author
                {
                  username
                }
                timestamp
                text
              }
            owner{
              username
            }
            
        }
        

    }
      
    
   }`;

   export const UPDATE_ROOM = gql`mutation updateRoom ($id: ID!, $name: String!) {
    updateRoom(id: $id, name: $name)
    {
        id
        name
       
    }
      
    
   }`;

   export const DELETE_ROOM = gql`mutation deleteRoom ($id: ID!) {
    deleteRoom(id: $id)
    {
        id
        name
       
    }
      
    
   }`;

   export const LEAVE_ROOM = gql`mutation leaveCurrentRoom{
    leaveCurrentRoom
    {
        id
        name
       
    }
      
    
   }`;

   export const MEMBER_JOINED = gql`subscription memberJoined {
    memberJoined
    {
        
        username
        

    }
      
    
   }`;

   export const MEMBER_LEFT = gql`subscription memberLeft {
    memberLeft
    {
        
        username
        

    }
      
    
   }`;

   export const MESSAGE_CREATED = gql`subscription messageCreated {
    messageCreated
    {
        
      id
      author
      {
        username
      }
      timestamp
      text
        

    }
      
    
   }`;

   export const CREATE_MESSAGE = gql`mutation createMessage($text: String!) {
    createMessage(text: $text)
    {
      id
      author
      {
        username
      }
      timestamp
      text
    }
      
    
   }`;