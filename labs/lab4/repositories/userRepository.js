const User = require('../models/user');



class UserRepository {
 
   
 
    async getUsers() { 
         
        try
        {
            const users = await User.find({});

            return users;

        }
        catch(err)
        {
            console.log(err);
        }
        
    }
 
    async getUserById(id) {
        try
        {
            const user = await User.findById(id);
        
            return user;

        }
        catch(err)
        {
            console.log(err);
        }
        

        
            
        
    }
};
 
module.exports = UserRepository;
