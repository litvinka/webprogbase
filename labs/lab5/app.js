const mustache = require('mustache-express');

const path = require('path');

const apiRouter = require('./routes/api');
const express = require('express');
const http = require('http');
const app = express();

const busboy = require('busboy-body-parser');

const options_busboy = {
   limit: '5mb',
   multi: false,
};

app.use(busboy(options_busboy));

const bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', apiRouter);

const morgan = require('morgan');
app.use(morgan('dev'));


const port = 3000


app.use(express.static('public'));

const viewsDir = path.join(__dirname, 'views');
const partialsDir = path.join(viewsDir, 'partials');




app.engine('mst', mustache(partialsDir));

app.set('views', viewsDir);
app.set('view engine', 'mst');


const server = http.createServer(app);
const WsServer = require('./websocketserver');
const wsServer = new WsServer(server);


app.use(function(req, res) {  console.log('Any request');});

server.listen(port, () => console.log('Server is ready'));



 

app.use(function (err, req, res, next) {
   console.error(err.stack);
   res.status(500).send('Something went wrong!');
})









