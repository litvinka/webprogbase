
/**
 * @typedef User
 * @property {string} id
 * @property {string} login.required - unique username
 * @property {string} fullname - name and surname of the user
 * @property {integer} role.required - 1 for admins, 0 for regular users
 * @property {string} registeredAt.required - date of user registration
 * @property {string} avaUrl - URL adress of profile picture
 * @property {boolean} isEnabled.required - shows if profile is active
 * @property {string} bio - optional biography of user
 */

const mongoose = require('mongoose');
const UserSchema = require('./../schemas/user');

 const UserModel = mongoose.model('User', UserSchema);


 
  
 
  
 
 
 module.exports = UserModel;
