


const Movie = require('../models/movie');
const MovieRepository = require('../repositories/movieRepository');
const movieRepository = new MovieRepository('./data/movies.json');

module.exports = 
{

    getMovies(req, res) {
        
        
        res.set("Content-type", "application/json");
        if (!('page' in req.query))
        {
            res.status(400);
            res.json({ });
            return;
        }
    
        const page = Number.parseInt(req.query.page);
        if(isNaN(page))
        {
            res.status(400);
            res.json({ });
            return;

        }

        if(page <= 0)
        {
            res.status(400);
            res.json({ });
            return;

        }
        if (!('per_page' in req.query))
        {
            res.status(400);
            res.json({ });
            return;
        }
    
        const per_page = Number.parseInt(req.query.per_page);
        if(isNaN(per_page))
        {
            res.status(400);
            res.json({ });
            return;
        }
        if(per_page <= 0)
        {
            res.status(400);
            res.json({ });
            return;

        }
        if(per_page > 100)
        {
            res.status(400);
            res.json({ });
            return;

        }
        
        const movies = movieRepository.getMovies();
        const items_total = movies.length;
        
        const pages_total = Math.ceil(items_total/per_page);
        if(page > pages_total)
        {
            res.status(404);
            res.json({ });
            return;
        }

        const first_index = per_page * (page-1);
        var i;
        var page_movies = new Array();
        

        for(i = first_index;i < first_index + per_page; i++)
        {

            if(i > (items_total - 1))
            {
                break;
            }
            
            page_movies.push(movies[i]);


        } 

     
        res.status(200);
        
        res.json(page_movies);


    },

    getMovieById(req, res) 
    {
        const id = parseInt(req.params.id);
        
        res.set("Content-type", "application/json");
        const movie = movieRepository.getMovieById(id);
        if(movie !== null)
        {
            res.status(200);
            
            res.json(movie);

        }
        else
        {
            
            res.status(404);
            res.json({ });

            
            
        }

    },

    createMovie(req, res) 
    {
        try
        {
            movie_object = req.body;
            if (!('title' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_title = movie_object.title;
            if (!('director' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_dir = movie_object.director;
            if (!('rating' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_rate = Number(movie_object.rating);
            if(!Number.isInteger(new_rate))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if((new_rate < 0)||(new_rate > 100))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if (!('releasedAt' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rel_date = movie_object.releasedAt
            const check_date = Date.parse(new_rel_date);
            if(isNaN(check_date))
            {
                res.status(400);
                res.json({ });
                return;
            }

            if (!('posterUrl' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_poster = movie_object.posterUrl;
            if (!('budget' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_budget = Number(movie_object.budget);
            
            if(isNaN(new_budget))
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(new_budget <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            const movie = new Movie(-1,new_title,new_dir,new_rate,new_rel_date,new_poster,new_budget);

            new_id = movieRepository.addMovie(movie);
            movie.id = new_id;
            
            
            res.status(201);
            res.json(movie);

        }
        catch(e)
        {
            
            console.log(e);
            res.status(400);
            res.json({ });

        }
        
    },
    updateMovie(req, res) {
        try
        {
            movie_object = req.body;
            if (!('id' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const id = movie_object.id;
            if(!Number.isInteger(id))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if(id <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if (!('title' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_title = movie_object.title;
            if (!('director' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_dir = movie_object.director;
            if (!('rating' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rate = Number(movie_object.rating);
            if(!Number.isInteger(new_rate))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if((new_rate < 0)||(new_rate > 100))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if (!('releasedAt' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rel_date = movie_object.releasedAt
            const check_date = Date.parse(new_rel_date);
            if(isNaN(check_date))
            {
                res.status(400);
                res.json({ });
                return;
            }

            if (!('posterUrl' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_poster = movie_object.posterUrl;
            if (!('budget' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_budget = Number(movie_object.budget);
            
            if(isNaN(new_budget))
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(new_budget <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(movieRepository.getMovieById(id) === null)
            {
                res.status(404);
                res.json({ });
                return;

            }
            
            const movie = new Movie(id,new_title,new_dir,new_rate,new_rel_date,new_poster,new_budget);

            movieRepository.updateMovie(movie);
            
            
            res.status(200);
            
            res.json(movie);

        }
        catch(e)
        {
            
            console.log(e);
            res.status(400);
            res.json({ });

        }

    },
    deleteMovieById(req, res) {
        const id = parseInt(req.params.id);
        
        res.set("Content-type", "application/json");
        const movie = movieRepository.getMovieById(id);
        if(movie !== null)
        {
            res.status(200);
            
            movieRepository.deleteMovie(id)
            res.json(movie);

        }
        else
        {
            
            res.status(404);
            res.json({ });
        }
        




    }

    
};

