const fs = require('fs');
class JsonStorage 
{

    // filePath - path to JSON file
    constructor(filePath) 
    {
        this.filePath = filePath;
    }

    get nextId() 
    {
       
        const jsonObject = this.readItems();
        return jsonObject.nextId;

    }

    incrementNextId() 
    {
        
        const jsonObject = this.readItems();

        jsonObject.nextId = jsonObject.nextId + 1;
        this.writeItems(jsonObject);
       
    }

    readItems() 
    {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonObject = JSON.parse(jsonText);
        return jsonObject;

    }

    writeItems(items) 
    {
        const jsonText = JSON.stringify(items,null,4);
        fs.writeFileSync(this.filePath,jsonText);
        
    }
};

module.exports = JsonStorage;