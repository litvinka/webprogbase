
function compare(movie_a, movie_b) {
    if (movie_a.title < movie_b.title) {
      return -1;
    }
    if (movie_a.title > movie_b.title) {
      return 1;
    }
    
    return 0;
  }

const cons = require('consolidate');
const Movie = require('../models/movie');
const MovieRepository = require('../repositories/movieRepository');
const movieRepository = new MovieRepository();

const MediaRepository = require('../repositories/mediaRepository');
const mediaRepository = new MediaRepository('./data/media.json');

module.exports = 
{

    async getMovies(req, res, next) {
        
        
        try
        {

        
            res.set("Content-type", "application/json");
            if (!('page' in req.query))
            {
                res.status(400);
                res.json({ });
                return;
            }
        
            const page = Number.parseInt(req.query.page);
            if(isNaN(page))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if(page <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if (!('per_page' in req.query))
            {
                res.status(400);
                res.json({ });
                return;
            }
        
            const per_page = Number.parseInt(req.query.per_page);
            if(isNaN(per_page))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if(per_page <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            if(per_page > 100)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            const movies = await movieRepository.getMovies();

            
            
        
            
            const items_total = movies.length;
            
            const pages_total = Math.ceil(items_total/per_page);
            if(page > pages_total)
            {
                res.status(404);
                res.json({ });
                return;
            }

            const first_index = per_page * (page-1);
            let i;
            let page_movies = new Array();
            

            for(i = first_index;i < first_index + per_page; i++)
            {

                if(i > (items_total - 1))
                {
                    break;
                }
                
                page_movies.push(movies[i]);


            } 

        
            res.status(200);
            
            res.json(page_movies);

        }

        catch(err)
        {
            next(err);
        }


    },

    async getMovieById(req, res, next) 
    {
        try
        {

        
            const id = req.params.id;
            
            res.set("Content-type", "application/json");
            
            
            const movie = await movieRepository.getMovieById(id);

            
            
            
            if(movie !== null)
            {
                res.status(200);
                
                res.json(movie);

            }
            else
            {
                
                res.status(404);
                res.json({ });

                
                
            }
        }
    catch(err)
        {
            next (err);
        }

    },

    async createMovie(req, res, next) 
    {
        try
        {   
            const new_title = req.body.title;

            const new_dir = req.body.director;
            
            const new_rate = Number(req.body.rating);
            
            const new_rel_date = req.body.release_date;

            const new_poster_url = await mediaRepository.addMedia(req.files.photoFile.data);

            const new_budget = Number(req.body.budget);
            
            const movie = new Movie({
                title: new_title, 
                director: new_dir,
                rating: new_rate,
                releasedAt: new_rel_date,
                posterUrl: new_poster_url,
                budget: new_budget
                
            });
            
            new_id = await movieRepository.addMovie(movie);

            path = 'movies/' + new_id;
            
            res.redirect(path);
            
        }
        catch(err)
        {
            next(err);
            

        }
        
    },
    async updateMovie(req, res) {
        try
        {
            movie_object = req.body;
            if (!('_id' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const id = movie_object.id;
            
            if (!('title' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_title = movie_object.title;
            if (!('director' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_dir = movie_object.director;
            if (!('rating' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rate = Number(movie_object.rating);
            if(!Number.isInteger(new_rate))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if((new_rate < 0)||(new_rate > 100))
            {
                res.status(400);
                res.json({ });
                return;

            }

            if (!('releasedAt' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rel_date = movie_object.releasedAt
            const check_date = Date.parse(new_rel_date);
            if(isNaN(check_date))
            {
                res.status(400);
                res.json({ });
                return;
            }

            if (!('posterUrl' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_poster = movie_object.posterUrl;
            if (!('budget' in movie_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_budget = Number(movie_object.budget);
            
            if(isNaN(new_budget))
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(new_budget <= 0)
            {
                res.status(400);
                res.json({ });
                return;

            }
            
            if(movieRepository.getMovieById(id) === null)
            {
                res.status(404);
                res.json({ });
                return;

            }
            
            const movie = new Movie({
                _id: id,
                title: new_title,
                director: new_dir,
                rating: new_rate,
                releasedAt: new_rel_date,
                posterUrl: new_poster,
                budget: new_budget

            });
            

            movieRepository.updateMovie(movie);
            
            
            res.status(200);
            
            res.json(movie);

        }
        catch(e)
        {
            
            console.log(e);
            res.status(400);
            res.json({ });

        }

    },
    async deleteMovieById(req, res, next) {
        try
        {
            const movie_id = req.params.id;
            
            const movie = await movieRepository.getMovieById(movie_id);
            if(movie !== null)
            {
                const poster_path = movie.posterUrl;

                let media_id_str = poster_path[59]; //first character of id

                for(i = 60; i < poster_path.length; i++)
                {
                    
                        media_id_str = media_id_str + poster_path[i];
                        
                    

                }
                await mediaRepository.deleteMedia(media_id_str);
                await movieRepository.deleteMovie(movie_id)

                res.redirect('/movies');
                

            }
            else
            {
                
                res.status(404);
                
            }

        }
        catch(err)
        {
            next(err);
        }
        
        




    },

    async load_movies_page(req,res,next)
    {
        try
        {

        
            let page;
            if (!('page' in req.query))
            {
                page = 1;
            }

            else
            {
                page = parseInt(req.query.page);

            }
        

            if (('next' in req.query))
            {
                page = page + 1;
            }

            if (('prev' in req.query))
            {   
                if(page !== 1)
                {
                    page = page - 1;
                }
            }
            const searched_string = req.query.title_search;
            
            const all_movies = await movieRepository.getMovies();
            let movies = [];
            if(searched_string != undefined)
            {
                for (const movie of all_movies)
                {
                
                    if(movie.title.toLowerCase().indexOf(searched_string.toLowerCase()) !== -1)
                    {
                        
                        movies.push(movie);

                    }
                    

                }
            }
            else
            {
                movies = all_movies
                

            }
            
            movies.sort(compare);
            const items_total = movies.length;
            const per_page = 5;
            const pages_total = Math.ceil(items_total/per_page);

            if(page > pages_total)
            {
                page = pages_total;
            }

            const first_index = per_page * (page-1);
            let i;
            let page_movies = new Array();
            

            for(i = first_index;i < first_index + per_page; i++)
            {

                if(i > (items_total - 1))
                {
                    break;
                }
                
                page_movies.push(movies[i]);


            } 

            let is_undefined = false;
            for(page_movie of page_movies)
            {
                if(typeof page_movie === 'undefined')
                {
                    is_undefined = true;
                    break;
                }
            }
            if(!(is_undefined))
            {
                movies = page_movies;
            }
            
            
            
            
            res.render('movies', {movies,searched_string, page, pages_total});
        }
        catch(err)
        {
            next(err);

        }
    },
    
    async load_movie_page(req,res, next)
    {   
        
        try
        {
            const id = req.params.id;
            if(id === 'new')
            {
                return next();
            }
            const movie = await movieRepository.getMovieById(id);
            
            res.render('movie', movie);

        }
        catch(err)
        {
            next(err);
        }
        
    },

    load_new_movie_page(req,res)
    {   
        let cur_date_string = new Date().getFullYear().toString() + "-" + new Date().getMonth().toString()+ "-" + new Date().getDate().toString()
        const cur_date = {cur_date_string: cur_date_string};
        res.render('new_movie', cur_date);

    }
    

    
};

