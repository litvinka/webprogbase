const mongoose = require('mongoose');

 const PlaylistSchema = new mongoose.Schema({
    name: {type: String, required: true},
    user_id: {type: mongoose.mongo.ObjectId, ref: "User", required: true},
    movies: [{type: mongoose.mongo.ObjectId, ref: "Movie"}]
   
});

module.exports = PlaylistSchema;