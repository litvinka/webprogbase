const router = require('express').Router();

const playlistController = require('../controllers/playlists');

router.get('/:id', playlistController.load_playlist_page)

.post("/", playlistController.createPlaylist)

.post('/:id', playlistController.deletePlaylistById)

.get('/new/:user_id', playlistController.load_new_playlist_page);



module.exports = router;