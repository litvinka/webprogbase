/**
 * @typedef Media
 * @property {integer} id
 * @property {string} path.required - path to the file
 */


class Media
{
    constructor(id, path)
    {
        this.id = id;// integer
        this.path = path;// string
    }
}
module.exports = Media;