const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() { 
        const jsonObject = this.storage.readItems();
        return jsonObject.items;  
    }
 
    getUserById(id) {
        const items = this.getUsers();
        const new_id = items.findIndex(item => item.id === id);
        
            if (new_id !== -1) {
                const user = new User(items[new_id].id, items[new_id].login, items[new_id].fullname,items[new_id].role,items[new_id].registeredAt,items[new_id].avaUrl,items[new_id].isEnabled);
                

                return user;
            
        }
        return null;
    }
};
 
module.exports = UserRepository;
