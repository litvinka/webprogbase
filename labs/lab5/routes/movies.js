const router = require('express').Router();

const movieController = require('../controllers/movies');

router

/**
 * Get movie model by id
 * @route GET /api/movies/{id}
 * @group Movies - movie operations
 * @param {integer} id.path.required - id of the Movie - eg: 1
 * @returns {Movie.model} 200 - Movie object
 * @returns {Error} 404 - Movie not found
 */

    //.get('/:id(\\d+)', movieController.getMovieById)

    .get('/:id(\\d+)', movieController.load_movie_page)

    /**
 * Get a page with a number of movies
 * @route GET /api/movies
 * @group Movies - movie operations
 * @param {integer} page.query.required - page number
 * @param {integer} per_page.query.required - items per page
 * @returns {Array.<Movie>} 200 - a page with Movies
 * @returns {Error} 404 - page doesn't exist
 * @returns {Error} 400 - invalid query
 */

    .get("/", movieController.load_movies_page)
    .get("/", movieController.getMovies)

    /**
 * Post a new movie
 * @route POST /api/movies
 * @group Movies - movie operations
 * @param {Movie.model} Movie.body.required - new Movie object
 * @returns {Movie.model} 201 - added Movie object
 * @returns {Error} 400 - invalid request
 */

    .post("/", movieController.createMovie)

    /**
 * Update a movie
 * @route PUT /api/movies
 * @group Movies - movie operations
 * @param {Movie.model} Movie.body.required - Movie object to update
 * @returns {Movie.model} 200 - changed Movie object
 * @returns {Error} 404 - Movie not found
 * @returns {Error} 400 - invalid request
 */

    .put("/", movieController.updateMovie)

    /**
 * Delete a movie
 * @route DELETE /api/movies/{id}
 * @group Movies - movie operations
 * @param {integer} id.path.required - id of the Movie - eg: 1
 * @returns {Movie.model} 200 - deleted Movie object
 * @returns {Error} 404 - Movie not found
 */

    .delete('/:id(\\d+)', movieController.deleteMovieById)
    .post('/:id(\\d+)', movieController.deleteMovieById)


    .get('/new', movieController.load_new_movie_page);
   
module.exports = router;